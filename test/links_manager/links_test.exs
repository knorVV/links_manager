defmodule LinksManager.LinksTest do
  use ExUnit.Case

  alias LinksManager.Links

  describe "links" do
    @links [
      "https://ya.ru",
      "https://ya.ru?q=123",
      "funbox.ru",
      "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
    ]

    test "link/1. Add links to redis" do
      adding_result = Links.link(@links)

      domains_list =
        Redix.command!(:redis, ~w(ZRANGEBYSCORE domains -inf +inf))
        |> Jason.decode!()

      assert adding_result == {:ok, 1}
      assert domains_list == [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ]

      Redix.command!(:redis, ~w(del domains))
    end

    test "link/2. Get links from redis" do
      Links.link(@links)

      assert {:ok, domains_list} = Links.link("-inf", "+inf")
      assert domains_list == [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ]

      Redix.command!(:redis, ~w(del domains))
    end

    test "link/2. Get links from redis by time range" do
      time = Timex.now()

      first_links =Jason.encode!([
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ])

      second_links = Jason.encode!([
        "funbox.ru",
        "stackoverflow.com"
      ])
      
      first_time = time_handling(time)
      add_links_to_redis(first_time, first_links) # Добавляем первую пачку ссылок в redis

      assert {:ok, first_domains} = Links.link(time_handling(time, -1), first_time)
      assert first_domains == [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ]

      second_time = time_handling(time, 1)
      add_links_to_redis(second_time, second_links) # Добавляем вторую пачку ссылок в redis

      assert {:ok, second_domains} = Links.link(second_time, time_handling(time, 2))
      assert second_domains == [
        "funbox.ru",
        "stackoverflow.com"
      ]

      assert {:ok, domains_list} = Links.link("-inf", "+inf")
      assert domains_list == [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ]

      Redix.command!(:redis, ~w(del domains))
    end

    defp add_links_to_redis(time, links) do
      Redix.command(:redis, ~w(ZADD domains CH #{time} #{links}))
    end

    defp time_handling(time) do # Переводим в формат unix
      time
      |> Timex.to_unix()
    end

    defp time_handling(time, shift) do # Сдвигаем время и переводим в формат unix
      time
      |> Timex.shift(seconds: shift)
      |> time_handling()
    end
  end
end
