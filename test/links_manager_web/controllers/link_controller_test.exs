defmodule LinksManagerWeb.LinkControllerTest do
  use LinksManagerWeb.ConnCase

  alias LinksManager.Links

  @links %{
    "links" =>
      [
        "https://ya.ru",
        "https://ya.ru?q=123",
        "funbox.ru",
        "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
      ]
  }
  
  describe "Link controller" do
    
    test "links_save/2", %{conn: conn}do
      assert %{
        "status" => "ok"
      } = conn
        |> post(Routes.link_path(conn, :links_save, @links))
        |> json_response(200)

      Redix.command!(:redis, ~w(del domains))
    end

    test "links_lookup/2", %{conn: conn}do
      add_links_to_redis()

      query = %{
        "from" => "-inf",
        "to" => "+inf"
      }

      %{
        "domains" => domains,
        "status" => "ok"
      } = conn
        |> get(Routes.link_path(conn, :links_lookup, query))
        |> json_response(200)

      assert domains == [
        "ya.ru",
        "funbox.ru",
        "stackoverflow.com"
      ]

      Redix.command!(:redis, ~w(del domains))
    end

    defp add_links_to_redis() do
      Links.link(@links["links"])
    end
  end
end
