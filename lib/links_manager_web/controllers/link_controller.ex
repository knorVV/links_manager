defmodule LinksManagerWeb.LinkController do
  use LinksManagerWeb, :controller

  alias LinksManager.Links

  action_fallback LinksManagerWeb.FallbackController

  require Logger

  def links_save(conn, %{"links" => links}) do
    case Links.link(links) do
      {:ok, _links} ->
        render(conn, "ok.json")

      {:error, error} ->
        Logger.error("Error during saving links. Error: #{inspect error})")
        render(conn, "error.json", %{error: error})
    end
  end

  def links_lookup(conn, %{"from" => time_from, "to" => time_to}) do
    case Links.link(time_from, time_to) do
      {:ok, domains} ->
        render(conn, "ok.json", %{domains: domains})

      {:error, error} ->
        Logger.error("Redis data receiving error. Error: #{inspect error})")
        render(conn, "error.json", %{error: error})

      error ->
        Logger.error("Undefined error during receiving data from redis. Error: #{inspect error})")
        render(conn, "error.json", %{error: error})
    end
  end
end
