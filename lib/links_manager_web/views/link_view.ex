defmodule LinksManagerWeb.LinkView do
  use LinksManagerWeb, :view

  def render("ok.json", %{domains: domains}) do
    %{
      domains: domains,
      status: :ok
    }
  end

  def render("ok.json", _) do
    %{status: :ok}
  end

  def render("error.json", %{error: error}) do
    %{status: error}
  end
end
