defmodule LinksManager.Links do
  @moduledoc """
    The Links context.
  """

  require Logger

  @doc """
    Сохранение ссылок в redis.
  """
  @spec link(List.t()) :: {:ok, String.t()} | {:error, any()}
  def link(links) when is_list(links) do
    {:ok, domains} = link_processing(links) # Парсим и пакуем ссылки в json

    time = get_current_unix_time() # Текущее время в формате unix

    Redix.command(:redis, ~w(ZADD domains CH #{time} #{domains})) # Сохраняем данные в redis
  end

  @doc """
    Получаем данные из редис по указанному диапазону
    time_from - время начала диапазона в формате unix
    time_to - конечное время диапазона в формате unix
  """
  @spec link(String.t(), String.t()) :: {:ok, List.t()} | {:error, any()}
  def link(time_from, time_to) do
    query_result =
      Redix.command(:redis, ~w(ZRANGEBYSCORE domains #{time_from} #{time_to}))

    redis_response_interpretation(query_result) # интерпретируем ответ от redis
  end

  # Получаем текущее время в формате unix
  # Можно вынести в отдельный модуль. Но приложение простое, поэтому это здесь
  @spec get_current_unix_time() :: Integer.t()
  defp get_current_unix_time() do
    Timex.now()
    |> Timex.to_unix()
  end

  # Получаем список хостов и пакуем в json
  @spec link_processing(List.t()) :: {:ok, String.t()} | {:error, any()}
  defp link_processing(links) do
    links
    |> parsing_links() # Получаем список хостов
    |> Jason.encode() # Пакуем список в json
  end

  # Получаем список хостов
  @spec parsing_links(List.t()) :: List.t()
  defp parsing_links(links) do
    links
    |> Enum.map(&get_host/1) # Получаем хосты из каждой ссылки в списке
    |> Enum.uniq() # Оставляяем уникальные значения
    |> Enum.filter(&is_host?/1) # Убираем nil
  end

  # Достаем хост из ссылки
  @spec get_host(String.t()) :: Sting.t()
  defp get_host(link) do
    case URI.parse(link) do
      %URI{host: nil} = uri ->
        %URI{path: path} = uri
        parsing_path(path)

      %URI{host: host} ->
        host
    end
  end

  # Проверяем не является ли path хостом
  # и возвращаем его если это так.
  # Проверяется на наличие точки в URL
  # Если  точка имеется, то добавляется в начало "//"
  @spec parsing_path(nil) :: nil
  defp parsing_path(nil), do: nil

  @spec parsing_path(String.t()) :: String.t()
  defp parsing_path(path) do
    case String.split(path, ".") do
      [_ | _] ->
        %URI{host: host} = URI.parse("//#{path}")
        host
      _ -> nil
    end
  end

  # Проверяем path != nil
  @spec is_host?(Sting) :: true | false
  defp is_host?(host), do: host != nil

  # Интерпретация ответа от redis
  @spec redis_response_interpretation({:ok, List.t()}) :: List.t()
  defp redis_response_interpretation({:ok, []}), do: {:ok, []}

  defp redis_response_interpretation({:ok, links}) do
    link_list =
      links
      |> Enum.map(&(Jason.decode!(&1)))
      |> List.flatten()
      |> Enum.uniq()

    {:ok, link_list}
  end

  @spec redis_response_interpretation(any()) :: any()
  defp redis_response_interpretation(error_response), do: error_response
end
