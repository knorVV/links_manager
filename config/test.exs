use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :links_manager, LinksManagerWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn
# Redis config
config :links_manager, redix: [
  host: "localhost",
  port: 6379
]
