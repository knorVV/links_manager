### Links manager
  Приложение для сохранения хостов и просмотра сохраненных хостов

### Что нужно для запуска.
  1. Установить и настроить redis в docker
  2. Запустить redis в docker контейнере`sudo docker run --name redis -p 6379:6379 -d redis`
  3. Запустить контейнер `sudo docker start #{номер контейнера}`
  4. Склонировать `git clone git@bitbucket.org:knorVV/links_manager.git`
  5. Перейти в директорию приложения. `cd links_manager`
  6. Загрузить зависимости`mix.deps.get`
  7. Запустить приложение`mix phx.server`

### Api (Локальные)
  1. Api для загрузки посещений `POST localhost:4000/visited_links`
    Пример формата для отправляемых параметров 
      {
        "links": [
          "https://ya.ru",
          "https://ya.ru?q=123",
          "funbox.ru",
          "https://stackoverflow.com/questions/11828270/how-to-exit-the-vim-editor"
        ]
      }

    Ответ на запрос:
      При успешном сохранении:
        {
          "status": "ok"
        }
      При ошибке во время сохранения:
        {
          "status": #{причина ошибки}
        }

  2. Api для получения статистики `GET localhost:4000/visited_domains?from=#{время в формате unix}&to=#{время в формате unix}`
    Ответ на запрос:
      При успешном запросе:
        {
          "domains": [
            "ya.ru",
            "funbox.ru",
            "stackoverflow.com"
          ],
          "status": "ok"
        }

        {
          "domains": []
          "status": "ok"
        }
      При ошибке во время запроса:
        {
          "status": #{причина ошибки}
        }
